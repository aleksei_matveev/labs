package zadanie4;
/**
 * 4.	Минимум двух чисел. Ввести с клавиатуры два числа, и вывести на экран минимальное из них
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class zadanie4 {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());

        System.out.println(min(a,b));

    }
    public static int min(int a, int b) {
        if (a < b) {
            return a;
        }else return b;
    }
}