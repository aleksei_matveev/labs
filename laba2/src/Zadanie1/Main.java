package Zadanie1;

/**
 * 1.	Четные числа Используя цикл for вывести на экран чётные числа от 1 до 100 включительно.
 * Через пробел либо с новой строки.
 */

class Zadanie1{

    public static void main(String[] args){

    int i;
        for(i=2; i<=100; i=i+2){
            System.out.print(i+" ");
        }

    }
}
