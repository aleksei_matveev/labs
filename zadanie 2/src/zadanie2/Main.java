package zadanie2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * 	2. Рисуем прямоугольник Ввести с клавиатуры два числа m и n.
 * Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
 */

class zadanie2 {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int k,m;
        for(k=0;b>k;k++){
            for (m=0;a>m;m++){
                System.out.print("8");
            }
            System.out.println();
        }
    }
}
